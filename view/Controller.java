package controller;

import model.*;
import view.*;

public class Controller {
    public static SimulatorFrame frame;

    public static void main(String[] args) {
        frame = new SimulatorFrame();
        frame.addSimulateListener(e -> {
            try{
                int n = frame.getNbClients();
                int q = frame.getNbQueues();
                int simulationTime = frame.getSimulationInterval();
                int minArrivalTime = frame.getMinArrivalTime();
                int maxArrivalTime = frame.getMaxArrivalTime();
                int minServiceTime = frame.getMinServiceTime();
                int maxServiceTime = frame.getMaxServiceTime();
                if (n<0 || q<0 || simulationTime < 0 || minArrivalTime < 0 || maxArrivalTime < 0 || minServiceTime < 0 || maxServiceTime < 0)
                    frame.setError();
                else if (minArrivalTime>maxArrivalTime || minServiceTime>maxServiceTime)
                    frame.setError();
                else
                    (new Thread(new SimulationManager(n,q,simulationTime,minArrivalTime,maxArrivalTime,minServiceTime,maxServiceTime))).start();
            }catch (NumberFormatException nfe){
                frame.setError();
            }
        });
    }
}
