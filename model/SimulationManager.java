package model;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import static controller.Controller.*;
import static java.lang.Thread.sleep;

public class SimulationManager implements Runnable{
    private final int numberOfClients;
    private final int timeLimit;
    private final Scheduler scheduler;
    private final List<Task> generatedTasks;

    public static AtomicInteger numberOfClientsPassed;
    public static AtomicInteger numberOfClientsEntered;
    public static AtomicInteger[] serviceTimes;
    public static AtomicInteger[] waitingTimes;
    public static boolean endSimulation;

    public SimulationManager(int numberOfClients, int numberOfServers, int timeLimit, int minArrivalTime, int maxArrivalTime, int minProcessingTime, int maxProcessingTime) {
        this.numberOfClients = numberOfClients;
        this.timeLimit = timeLimit;
        scheduler = new Scheduler(numberOfServers);
        generatedTasks = new ArrayList<>();
        serviceTimes = new AtomicInteger[numberOfClients];
        waitingTimes = new AtomicInteger[numberOfClients];
        for (int i=0;i<serviceTimes.length;++i) {
            serviceTimes[i] = new AtomicInteger();
            waitingTimes[i] = new AtomicInteger();
        }
        for (int i=1;i<=numberOfClients;++i){
            int processingTime = (int)((Math.random())*(maxProcessingTime-minProcessingTime+1)+minProcessingTime);
            int arrivalTime = (int)((Math.random())*(maxArrivalTime-minArrivalTime+1)+minArrivalTime);
            generatedTasks.add(new Task(i,arrivalTime,processingTime));
        }
        numberOfClientsPassed = new AtomicInteger();
        numberOfClientsEntered = new AtomicInteger();
        endSimulation = false;
    }

    @Override
    public void run() {
        try {
            // cream fisierul log.txt
            String path = "src/model/events.txt";
            File eventsLog = new File(path);
            eventsLog.createNewFile();
            // stergem tot continutul din fisier
            PrintWriter pw = new PrintWriter(path);
            pw.print("");
            pw.close();
            // incepem sa scriem in fisier
            FileWriter fw = new FileWriter(path);
            int peakHour = -1;
            int maxNumberOfClientsWaiting = -1;
            int currentTime = 0;

            while(!endSimulation){
                fw.write("Time "+currentTime+"\nWaiting clients:");
                Iterator<Task> it = generatedTasks.iterator();
                while(it.hasNext()) {
                    Task t = it.next();
                    if (t.getArrivalTime() == currentTime) {
                        scheduler.dispatchTask(t);
                        it.remove();
                    }
                }
                for (Task t : generatedTasks)
                    fw.write(" "+t+";");
                fw.write("\n");

                int idx = 0;
                int numberOfClientsWaiting = 0;

                for (Server s : scheduler.getServers()){
                    ++idx;
                    fw.write("Queue "+idx+":");
                    for (Object t : s.getTasks()) {
                        ++numberOfClientsWaiting;
                        fw.write(" " + t + ";");
                    }
                    fw.write("\n");
                }

                if (maxNumberOfClientsWaiting<numberOfClientsWaiting){
                    maxNumberOfClientsWaiting=numberOfClientsWaiting;
                    peakHour = currentTime;
                }

                int totalWaitingTime = 0;
                for (AtomicInteger ai : waitingTimes)
                    totalWaitingTime += ai.get();

                int totalServiceTime = 0;
                for (AtomicInteger ai : serviceTimes)
                    totalServiceTime += ai.get();

                fw.write("Average waiting time: "+(float)(totalWaitingTime+totalServiceTime)/(float)numberOfClientsEntered.get());
                fw.write("\nAverage service time: "+(float)totalServiceTime/(float)numberOfClientsPassed.get());
                fw.write("\nPeak hour "+peakHour);
                fw.write("\nTotal time spent by every client:");
                for (int i=0;i<numberOfClients;++i) {
                    int totalTimeSpent = waitingTimes[i].get() + serviceTimes[i].get();
                    fw.write(" " + i + " - " + totalTimeSpent + " s; ");
                }
                fw.write("\n\n");

                frame.updateEventLog(currentTime,generatedTasks,scheduler.getServers());
                ++currentTime;
                boolean emptyQueues = true;
                for (Server s : scheduler.getServers())
                    if (s.getTasks().length > 0)
                        emptyQueues = false;
                if (currentTime>timeLimit || (generatedTasks.isEmpty() && emptyQueues))
                    endSimulation = true;
                sleep(1000);
            }
            fw.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}