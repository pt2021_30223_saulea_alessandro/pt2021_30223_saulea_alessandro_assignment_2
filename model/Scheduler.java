package model;
import static model.SimulationManager.*;

public class Scheduler {
    private final Server[] servers;

    public Scheduler(int maxNoServers) {
        servers = new Server[maxNoServers];
        for (int i=0;i<maxNoServers;++i){
            servers[i] = new Server();
            (new Thread(servers[i])).start();
        }
    }

    public void dispatchTask(Task t){
        numberOfClientsEntered.incrementAndGet();
        Server shortestServer = servers[0];
        for (Server s : servers)
            if (shortestServer.getWaitingPeriod()>s.getWaitingPeriod())
                shortestServer = s;
        shortestServer.addTask(t);
    }

    public Server[] getServers() {
        return servers;
    }
}
