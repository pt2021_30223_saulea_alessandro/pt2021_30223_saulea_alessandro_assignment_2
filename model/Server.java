package model;

import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import static model.SimulationManager.*;
import static java.lang.Thread.sleep;

public class Server implements Runnable{
    private final BlockingQueue<Task> tasks;
    private final AtomicInteger waitingPeriod;

    public Server() {
        tasks = new LinkedBlockingQueue<>();
        waitingPeriod = new AtomicInteger(0);
    }

    public void addTask(Task newTask){
        tasks.add(newTask);
        waitingPeriod.addAndGet(newTask.getProcessingTime());
    }

    @Override
    public void run() {
        while(!endSimulation){
            for (Task t : tasks)
                if (t!=tasks.peek())
                    waitingTimes[t.getId() - 1].incrementAndGet();
            if (!tasks.isEmpty()) {
                numberOfClientsPassed.incrementAndGet();
                for (int i=0;i<tasks.peek().getProcessingTime() && !endSimulation;++i){
                    serviceTimes[tasks.peek().getId()-1].incrementAndGet();
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    waitingPeriod.decrementAndGet();
                }
                tasks.poll();
            }
        }
    }

    public Object[] getTasks() {
        return tasks.toArray();
    }

    public int getWaitingPeriod() {
        return waitingPeriod.get();
    }
}
